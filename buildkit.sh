#!/bin/bash
set -e

if [ $# -lt 1 ]; then 
    echo "Usage: $0 <image tag> <cache>"
    exit 1
fi

destination=$1

cache="true" #--export-cache --import-cache
if [[ ! -z "$2" ]]; then
    cache=$2
fi

if [[ ! -e $HOME/.config/gcloud/application_default_credentials.json ]]; then
    echo "Application Default Credentials do not exist. Run [gcloud auth application-default login] to configure them"
    exit 1
fi

docker run \
    -it \
    --rm \
    --privileged \
    -v $(pwd):/workspace -v $(pwd)/configjson:/root/.docker \
    --entrypoint buildctl-daemonless.sh \
    moby/buildkit:master \
    build --frontend dockerfile.v0 --local context=/workspace --local dockerfile=/workspace --local config=/workspace --output type=image,name=${destination},push=true
