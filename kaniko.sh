#!/bin/bash
set -e

if [ $# -lt 1 ]; then 
    echo "Usage: $0 <image tag> <cache>"
    exit 1
fi

destination=$1

cache="false" #true
if [[ ! -z "$2" ]]; then
    cache=$2
fi

if [[ ! -e $HOME/.config/gcloud/application_default_credentials.json ]]; then
    echo "Application Default Credentials do not exist. Run [gcloud auth application-default login] to configure them"
    exit 1
fi

docker run \
    -v $HOME/.config/gcloud:/root/.config/gcloud -v $(pwd):/workspace \
    gcr.io/kaniko-project/executor:latest \
    --dockerfile Dockerfile --destination ${destination} --context dir:///workspace/ --cache=${cache}
