#!/bin/bash
set -e

if [ $# -lt 1 ]; then 
    echo "Usage: $0 <image tag> <cache>"
    exit 1
fi

destination=$1

cache="true" #--no-cache
if [[ ! -z "$2" ]]; then
    cache=$2
fi

if [[ ! -e $HOME/.config/gcloud/application_default_credentials.json ]]; then
    echo "Application Default Credentials do not exist. Run [gcloud auth application-default login] to configure them"
    exit 1
fi

docker run \
    --entrypoint /usr/local/bin/s2i \
    -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd)/gen-source:/gen-source -v $(pwd):/workspace \
    quay.io/openshift-pipeline/s2i \
    build /workspace/s2i centos/php-72-centos7 --as-dockerfile /gen-source/Dockerfile

docker run \
    --privileged \
    -v varlibcontainers:/var/lib/containers -v $(pwd):/workspace \
    quay.io/buildah/stable \
    buildah bud -t ${destination} --layers /workspace/gen-source

docker run \
    --privileged \
    -v varlibcontainers:/var/lib/containers -v $(pwd)/configjson:/root/.docker \
    quay.io/buildah/stable \
    buildah push ${destination}

