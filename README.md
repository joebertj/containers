# containers

Demonstration of containers and different implementations

## Container Registry

### Private Registry
```
docker run -d -p 5000:5000 registry
docker build -t localhost:5000/local .
docker push localhost:5000/local
docker run -d -p 80:80 localhost:5000/local
```

### Authenticating to the Docker registry
- Using `docker login`
- Using Docker credential helper

### Credential Helpers
- `docker-credential-ecr-login`
- `docker-credential-gcr`

### Editing Docker Config manually
`cat $HOME/.docker/config.json`

```
{
	"credHelpers": {
		"784571545063.dkr.ecr.ap-southeast-1.amazonaws.com": "ecr-login",
		"asia.gcr.io": "gcr",
		"eu.gcr.io": "gcr",
		"gcr.io": "gcr",
		"marketplace.gcr.io": "gcr",
		"staging-k8s.gcr.io": "gcr",
		"us.gcr.io": "gcr"
	}
}
```

## GCP Setup

### Authenticate to GCloud
`gcloud auth application-default login`

### Enable GCR API
https://console.cloud.google.com/apis/api/containerregistry.googleapis.com/overview?project=container-254002

### Install GCR Credential Helper
```
wget https://github.com/GoogleCloudPlatform/docker-credential-gcr/releases/download/v1.5.0/docker-credential-gcr_linux_amd64-1.5.0.tar.gz
tar xvzf docker-credential-gcr_linux_amd64-1.5.0.tar.gz
sudo mv docker-credential-gcr /usr/local/bin
```

### Generate Docker config
`gcloud auth configure-docker`

## Containers

### Docker daemon vs Containerd daemon
`netstat | grep docker`  
`netstat | grep containerd`

## Docker Lifecycle

### Building and Pushing Docker Containers
- Run using local docker   
`./docker.sh asia.gcr.io/container-254002/docker:0.1`  
- Run using docker in docker   
`./dind.sh asia.gcr.io/container-254002/dind:0.1`  
- Run using local podman   
`./podman.sh asia.gcr.io/container-254002/podman:0.1`  
- Run using local buildkit   
`./buildkit-local.sh asia.gcr.io/container-254002/buildkit:0.1`  
- Run using local buildah   
`./buildah-local.sh asia.gcr.io/container-254002/buildah:0.1`  
- Run using kaniko   
`./kaniko.sh asia.gcr.io/container-254002/kaniko:0.1`  
- Run using buildah   
`./buildah.sh asia.gcr.io/container-254002/buildah:0.1`  
- Run using buildkit   
`./buildkit.sh asia.gcr.io/container-254002/buildkit:0.1`  
- Run using s2i   
`./s2i.sh asia.gcr.io/container-254002/s2i:0.1`  

## Listing Docker Images
`docker images`

## Deleting Docker Images
`docker rmi`

### Running Docker Containers
`docker run -d -p 80:80 asia.gcr.io/container-254002/docker:0.1`  
`docker run -d -p 80:8080 -p 443:8443 asia.gcr.io/container-254002/s2i:0.1`

### Listing Running Docker Containers
`docker ps`

### Stopping Docker Containers
`docker stop`

### Deleting Docker Containers
`docker rm`

### Prerequisites
- Docker auth credentials (expires for some time)  
`gcloud auth print-access-token | docker login -u oauth2accesstoken --password-stdin https://asia.gcr.io`  
- Install podman  
https://github.com/containers/libpod/blob/master/install.md  
- Install buildah  
https://github.com/containers/buildah/blob/master/install.md  
- Setup buildah config  
```
cat <<EOF > registries.conf
[registries.search]
registries = ['docker.io', 'registry.fedoraproject.org', 'registry.access.redhat.com']
[registries.insecure]
registries = []
[registries.block]
registries = []
EOF
sudo mv registries.conf /etc/containers/registries.conf
```

## runc
```
mkdir mycontainer
cd mycontainer/
mkdir rootfs
docker export $(docker create busybox) | tar -C rootfs -xvf -
runc spec
sudo runc run mycontainerid
sudo runc create mycontainerid
sudo runc list
runc start mycontainerid
sudo runc list
sudo runc delete mycontainerid
```
### Rootless
```
runc spec --rootless
runc --root /tmp/runc run mycontainerid
```

##namespaces

### pid namespace
```
sudo unshare --fork --pid --mount-proc bash
```

### docker via namespace
```
docker export cb736060627a -o nginx.tar
mkdir rootfs
tar xf nginx.tar -C rootfs
unshare --mount --uts --ipc --net --pid --fork --user --map-root-user chroot $PWD/rootfs /bin/sh
```
