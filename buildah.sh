#!/bin/bash
set -e

if [ $# -lt 1 ]; then 
    echo "Usage: $0 <image tag> <cache>"
    exit 1
fi

destination=$1

cache="--no-cache" #--layers
if [[ ! -z "$2" ]]; then
    cache=$2
fi

if [[ ! -e $HOME/.config/gcloud/application_default_credentials.json ]]; then
    echo "Application Default Credentials do not exist. Run [gcloud auth application-default login] to configure them"
    exit 1
fi

docker run \
    --privileged \
    -v varlibcontainers:/var/lib/containers -v $(pwd):/workspace \
    quay.io/buildah/stable \
    buildah bud -t ${destination} --no-cache /workspace

docker run \
    --privileged \
    -v varlibcontainers:/var/lib/containers -v $(pwd)/configjson:/root/.docker \
    quay.io/buildah/stable \
    buildah push ${destination}
