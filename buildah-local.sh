#!/bin/bash
set -e

if [ $# -lt 1 ]; then 
    echo "Usage: $0 <image tag> <cache>"
    exit 1
fi

destination=$1

cache="--no-cache" #--layers
if [[ ! -z "$2" ]]; then
    cache=$2
fi

if [[ ! -e $HOME/.config/gcloud/application_default_credentials.json ]]; then
    echo "Application Default Credentials do not exist. Run [gcloud auth application-default login] to configure them"
    exit 1
fi

buildah bud \
    -t ${destination} --no-cache .

buildah push \
    ${destination}
